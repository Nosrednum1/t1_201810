package model.logic;

import java.util.ArrayList;
import java.util.Iterator;

import model.data_structures.NumbersBag;

public class NumbersBagOperations<N extends Number>  {

	/**
	 * M�todo para comparar dos n�meros
	 * @param thisValue Primer valor de comparaci�n
	 * @param whitThis Segundo valor de comparaci�n
	 * @return <b>1</b> si el <i>primer valor es <u>mayor</u></i> que el segundo<br>
	 * <b>-1</b> si el <i>primer valor es <u>menor</u></i> que el segundo <br>
	 *  o <br><b>0</b> si <i>son <u>iguales</u></i>
	 */
	private <N>int compareTo(N thisValue, N whitThis){
		if((double)thisValue > (double)whitThis)return 1;
		else if((double)thisValue < (double)whitThis)return -11;
		else return 0;
	}

	/**
	 * M�todo para calcular la medioa de <b>todos</b> los valores en la <i><b>NumersBag</b></i>
	 * @param bag <i>NumersBag</i> con los valores para calcular el resultado
	 * @return la media de los valores de la <i><b>NumersBag</b></i> o 0 si �sta est� vac�a
	 */
	public <N>double computeMean(NumbersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<N> iter = bag.getIterator();
			while(iter.hasNext()){
				double toSum = (double)iter.next();
				mean += toSum;
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}

	/**
	 * M�todo para hallar el m�ximo valor en la <i><b>NumersBag</b></i>
	 * @param bag <i><b>NumersBag</b></i>
	 * @return el m�ximo valor o 0 si est� vac�a o su valor es 0
	 */
	public <N extends Number> double getMax(NumbersBag<N> bag){
		double max = 0; 
		if(bag != null){
			Iterator<N> iter = bag.getIterator();
			while(iter.hasNext()){
				double value = (double)iter.next();
				if(compareTo(value, max) > 0)max =  value;
			}}
		return max;
	}

	/**
	 * M�todo para hallar el m�nimo valor en la <i><b>NumersBag</b></i>
	 * @param bag <i><b>NumersBag</b></i>
	 * @return el m�nimo valor o 0 si est� vac�a o su valor es 0
	 */
	public <N extends Number> double getMin(NumbersBag<N> bag){
		double min = Integer.MAX_VALUE; 
		if(bag != null){
			Iterator<N> iter = bag.getIterator();
			while(iter.hasNext()){
				double value = (double)iter.next();
				if(compareTo(value, min) < 0)min =  value;
			}}
		return min;
	}


	/**
	 * M�todo para hallar la cantidad de elementos mayores al pasado por par�metro
	 * @param bag <i><b>NumersBag</b></i>
	 * @param value valor base a comparar
	 * @return 0 si no hay valores m�s grandes o la <i><b>NumersBag</b></i> est� vac�a
	 */
	public <N> int greaterThan(NumbersBag bag, N value){
		int counter = 0;
		N val;
		if(bag != null){
			Iterator<N> iter = bag.getIterator();
			while(iter.hasNext()){
				val = iter.next();
				if( compareTo(val, value) > 0)counter ++;
			}
		}
		return counter;
	}

	/**
	 * M�todo para hallar la cantidad de elementos mayores al pasado por par�metro
	 * @param bag <i><b>NumersBag</b></i>
	 * @param value valor base a comparar
	 * @return un arreglo con los elemento mayores o un arrreglo vac�o en caso de que no haya elementos
	 */
	public <N> ArrayList<N> greaterThanX(NumbersBag bag, N number) {
		int counter = 0;
		N val;
		ArrayList<N> a = new ArrayList<>();
		if(bag != null){
			Iterator<N> iter = bag.getIterator();
			while(iter.hasNext()){
				val = iter.next();
				if( compareTo(val, number) > 0){
					counter ++; a.add(val);
				}
			}
		}
		return a;
	}

	/**
	 * M�todo para hallar la cantidad de elementos menores al pasado por par�metro
	 * @param bag <i><b>NumersBag</b></i>
	 * @param value valor base a comparar
	 * @return 0 si no hay valores m�s "peque�os" o la <i><b>NumersBag</b></i> est� vac�a
	 */
	public <N> int smallerThan(NumbersBag bag, N value){
		int counter = 0;
		N val;
		if(bag != null){
			Iterator<N> iter = bag.getIterator();
			while(iter.hasNext()){
				val = iter.next();
				if( compareTo(val, value) < 0)counter ++;
			}
		}
		return counter;
	}

	/**
	 * M�todo para hallar la cantidad de elementos menores al pasado por par�metro
	 * @param bag <i><b>NumersBag</b></i>
	 * @param value valor base a comparar
	 * @return un arreglo con los elemento menores o un arrreglo vac�o en caso de que no haya elementos
	 */
	public <N> ArrayList<N> smallerThanX(NumbersBag bag, N number) {
		int counter = 0;
		N val;
		ArrayList<N> a = new ArrayList<>();
		if(bag != null){
			Iterator<N> iter = bag.getIterator();
			while(iter.hasNext()){
				val = iter.next();
				if( compareTo(val, number) < 0){
					counter ++; a.add(val);
				}
			}
		}
		return a;
	}

}
