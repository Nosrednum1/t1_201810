package controller;

import java.util.ArrayList;

import model.data_structures.NumbersBag;
import model.logic.NumbersBagOperations;

public class Controller<N extends Number> {

	private static NumbersBagOperations model = new NumbersBagOperations();


	public static <N> NumbersBag createBag(ArrayList<N> values){
		return new NumbersBag(values);		
	}


	public static double getMean(NumbersBag bag){
		return model.computeMean(bag);
	}

	public static double getMax(NumbersBag bag){
		return model.getMax(bag);
	}

	public static double getMin(NumbersBag bag){
		return model.getMin(bag);
	}

	public static <N> int getCountGreater(NumbersBag bag, N number){
		return model.greaterThan(bag, number);
	}

	public static <N> ArrayList<N> getGrater(NumbersBag bag, N number){
		return  model.greaterThanX(bag, number);
	}

	public static <N> int getCountSmaller(NumbersBag bag, N number){
		return model.smallerThan(bag, number);
	}

	public static <N> ArrayList<N> getSmaller(NumbersBag bag, N number){
		return  model.smallerThanX(bag, number);
	}

}
