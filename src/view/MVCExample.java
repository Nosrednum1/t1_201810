package view;

import java.util.ArrayList;
import java.util.Scanner;

import controller.Controller;
import model.data_structures.NumbersBag;

public class MVCExample {

	/**
	 * Método encargado de la escritura de las opciones que serán delplegadas en la consola
	 */
	private static void printMenu(){
		System.out.println("1. Create a new bag of numbers");
		System.out.println("2. Compute the mean");
		System.out.println("3. Get the max value");
		System.out.println("4. Get the min value");
		System.out.println("5. Get the amount of values greaters than...");
		System.out.println("6. Get the values greaters than...");
		System.out.println("7. Get the amount of values greaters than...");
		System.out.println("8. Get the values greaters than...");
		System.out.println("9. Exit");
		System.out.println("Type the option number for the task, then press enter: (e.g., 1):");
	}

	/**
	 *  Método encargado de recibir los números que quedarán dentro de la <b>NumbersBag</b>
	 * @return Lista que será usada para la creación de la <b><i>NumbersBag</i></b>
	 */
	private static <N extends Number> ArrayList<N> readData(){
		ArrayList<Double> data = new ArrayList<>();
		System.out.println("Please type the numbers (separated by spaces), then press enter: ");
		try{
			String line = new Scanner(System.in).nextLine();
			String[] values = line.split("\\s+");
			for(String value: values){
				data.add(new Double(value.trim()));
			}
		}catch(Exception ex){
			System.out.println("Please type numbers separated by spaces");
		}
		return (ArrayList<N>) data;
	}

	/**
	 * Método encargado de registrar el número base para comparar con los elementos que existen en la <b><i>NumbersBag</i></b> 
	 *  @author Anderson Barragán
	 * @return <b>double</b> el valor ingresado
	 */
	private static double base(){
		System.out.println("Type the base number:");
		String b = new Scanner(System.in).nextLine();
		double toRet = Double.parseDouble(b);
		return toRet;
	}

	private static <N> String printArray(ArrayList<N> a){
		String toRet = "";		
		for ( N elemento : a ) 
			toRet += " " + elemento;
		return toRet;
	}


	/**
	 * Método main...
	 * @param args
	 */
	public static void main(String[] args){

		NumbersBag bag = null;
		Scanner sc = new Scanner(System.in);

		for(;;){
			printMenu();

			int option = sc.nextInt();
			switch(option){
			case 1: bag = Controller.createBag(readData()); System.out.println("--------- \n The bag was created  \n---------");
			break;

			case 2: System.out.println("--------- \n The mean value is "+Controller.getMean(bag)+" \n---------");
			break;

			case 3: System.out.println("--------- \nThe max value is "+Controller.getMax(bag)+" \n---------");		  
			break;

			case 4 : System.out.println("--------- \nThe min value is "+Controller.getMin(bag)+" \n---------");
			break;

			case 5 : System.out.println("--------- \n the amount of larger values ​​is: "+Controller.getCountGreater(bag, base())+" \n---------");
			break;

			case 6 : System.out.println("--------- \n the amount of larger values ​​is: "+printArray(Controller.getGrater(bag, base()))+" \n---------");
			break;

			case 7 : System.out.println("--------- \n the amount of smaller values ​​is: "+Controller.getCountSmaller(bag, base())+" \n---------");
			break;

			case 8 : System.out.println("--------- \n the amount of smaller values ​​is: "+printArray(Controller.getSmaller(bag, base()))+" \n---------");
			break;

			case 9: System.out.println("Bye!!  \n---------"); sc.close(); return;		  

			default: System.out.println("--------- \n Invalid option !! \n---------");
			}
		}
	}
}
