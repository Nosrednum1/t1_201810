package model.data_structures;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;


public class NumbersBag <N extends Number>{

	private HashSet<N> bag;

	public NumbersBag(){
		this.bag = new HashSet<>();
	}

	public NumbersBag(ArrayList<N> data){
		this();
		if(data != null){
			for (N datum : data) {
				bag.add(datum);
			}
		}
	}

	public void addDatum(N datum){
		bag.add(datum);
	}

	public Iterator<N> getIterator(){
		return (Iterator<N>) this.bag.iterator();
	}

}
